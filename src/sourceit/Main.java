package sourceit;

import sourceit.sorter.SortMonth;
import sourceit.sorter.SortName;
import sourceit.sorter.SortYear;

import java.lang.reflect.Array;
import java.util.Scanner;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        TreeSet<Worker> workers = new TreeSet<>();
        workers.add(new Worker("Иванова Елена Львовна", "зам. диретора", 9500, 0, 0, 0, 0));
        workers.add(new Worker("Вакуленко Дмитрий Владимирович", "дизайнер", 0, 0, 0, 7, 0));
        workers.add(new Worker("Коренькова Анна Павловна", "менеджер по продажам", 0, 0, 0, 0, 5));
        workers.add(new Worker("Татьяна Сергеевна", "менеджер по продажам", 1000, 0, 0, 0, 3));
        workers.add(new Worker("Михаил", "программист", 0, 0, 0, 15, 0));
        workers.add(new Worker("Анна", "дизайнер-программист", 0, 0, 0, 17, 0));
        workers.add(new Worker("Петр", "дворник", 4500, 0, 0, 0, 0));
        workers.add(new Worker("Татьяна", "директор ФЛП", 5000, 0, 0, 0, 8));
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите варинат сортировки: 1 - по имени; 2 - по з/п в год; 3 - по з/п в месяц");
        int sort = Integer.parseInt(scanner.nextLine());
        if (sort == 1) {
            TreeSet<SortName> sortNames = new TreeSet<>();
            for (Worker i : workers) {
                System.out.println("Ф.И.О.:" + i.fuulName
                        + " Должность:" + i.getPlace()
                        + " Месячная ставка (приведенная в гривне): " + i.monthPaymentUAYear()
                        + " Часовая оплата (приведенная в гривне):" + i.hourPaymentUAYear()
                        + " Процент от объема продаж: " + i.persentVolumeSentUAYear()
                        + " Месячная ставка и процент от объема продаж (приведенная в гривне): " + i.hourPaymentUAYearPluspersentVolumeSentUAYear());
            }
        } else if (sort == 2) {
            TreeSet<SortYear> sortYears = new TreeSet<>();
            for (Worker i : workers) {
                System.out.println("Ф.И.О.:" + i.fuulName
                        + " Должность:" + i.getPlace()
                        + " Месячная ставка (приведенная в гривне): " + i.monthPaymentUAYear()
                        + " Часовая оплата (приведенная в гривне):" + i.hourPaymentUAYear()
                        + " Процент от объема продаж: " + i.persentVolumeSentUAYear()
                        + " Месячная ставка и процент от объема продаж (приведенная в гривне): " + i.hourPaymentUAYearPluspersentVolumeSentUAYear());
            }
        } else if (sort == 3) {
            TreeSet<SortMonth> sortMonths = new TreeSet<>();
            for (Worker i : workers) {
                System.out.println("Ф.И.О.:" + i.fuulName
                        + " Должность:" + i.getPlace()
                        + " Месячная ставка (приведенная в гривне): " + i.monthPaymentUAYear()
                        + " Часовая оплата (приведенная в гривне):" + i.hourPaymentUAYear()
                        + " Процент от объема продаж: " + i.persentVolumeSentUAYear()
                        + " Месячная ставка и процент от объема продаж (приведенная в гривне): " + i.hourPaymentUAYearPluspersentVolumeSentUAYear());

                System.out.println(workers);
            }
        } else {
            System.out.println("Ввод выполнен не коректно");
        }
    }
}

