package sourceit;


public class Worker implements Comparable {
    private final static int FIRSTHALFYEARUA = 50000 * 6;
    private final static int SECONDHALFYEARUA = 65000 * 6;
    private final static float KURSSELLUSD = 25.5f;
    private final static int HOURMONTH = 120;
    private final static int MONTHYEAR = 12;

    String fuulName;
    String place;
    int monthPaymentUA;
    int monthPaymentUSD;
    int hourPaymentUA;
    int hourPaymentUSD;
    float persentVolumeSentUA;

    public Worker(String fuulName, String place, int monthPaymentUA, int monthPaymentUSD, int hourPaymentUA, int hourPaymentUSD, float persentVolumeSentUA) {
        this.fuulName = fuulName;
        this.place = place;
        this.monthPaymentUA = monthPaymentUA;
        this.monthPaymentUSD = monthPaymentUSD;
        this.hourPaymentUA = hourPaymentUA;
        this.hourPaymentUSD = hourPaymentUSD;
        this.persentVolumeSentUA = persentVolumeSentUA;
    }

    @Override
    public int compareTo(Object o) {
        Worker entry = (Worker) o;
        int result = fuulName.compareTo(entry.fuulName);
        if (result != 0) {
            return result;
        }
        return 0;
    }

    public int monthPaymentUAYear() {
        float result = monthPaymentUSD * KURSSELLUSD * MONTHYEAR;
        if (monthPaymentUA == 0) {
            return (int) result;
        } else {
            return monthPaymentUA * MONTHYEAR;
        }
    }

    public int hourPaymentUAYear() {
        float result = hourPaymentUSD * KURSSELLUSD * HOURMONTH * MONTHYEAR;
        if (hourPaymentUA == 0) {
            return (int) result;
        } else {
            return hourPaymentUA * HOURMONTH * MONTHYEAR;
        }
    }

    public int persentVolumeSentUAYear() {
        return (int) ((persentVolumeSentUA * FIRSTHALFYEARUA) + (persentVolumeSentUA * SECONDHALFYEARUA));
    }

    public int hourPaymentUAYearPluspersentVolumeSentUAYear() {
        return persentVolumeSentUAYear() + monthPaymentUAYear() + hourPaymentUAYear();
    }

    public String getPlace() {
        return place;
    }

    public String getFuulName() {
        return fuulName;
    }
    public  int hourPaymentUAYearPluspersentVolumeSentUAMonth() {
        return hourPaymentUAYearPluspersentVolumeSentUAYear() / MONTHYEAR;
    }
    public int hourPaymentUAMont() {
        return hourPaymentUAYear() / MONTHYEAR;
    }

}


