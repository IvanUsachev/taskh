package sourceit.sorter;

import sourceit.Worker;

import java.util.Comparator;

public class SortName implements Comparator<Worker> {
    @Override
    public int compare(Worker o1, Worker o2) {
        String str1 = o1.getFuulName();
        String str2 = o2.getFuulName();
        return str1.compareTo(str2);
    }
}
