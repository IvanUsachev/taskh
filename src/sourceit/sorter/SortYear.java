package sourceit.sorter;

import sourceit.Worker;

import java.util.Comparator;

public class SortYear implements Comparator<Worker> {
    @Override
    public int compare(Worker o1, Worker o2) {
        int payment1 = o1.hourPaymentUAYearPluspersentVolumeSentUAYear() + o1.hourPaymentUAYear();
        int payment2 = o2.hourPaymentUAYearPluspersentVolumeSentUAYear() + o2.hourPaymentUAYear();
        if (payment1 > payment2) {
            return 1;
        } else if (payment1 < payment2) {
            return -1;
        } else {
            return 0;
        }
    }
}
